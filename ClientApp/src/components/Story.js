﻿import React, { Component } from 'react';

import './story.css'

export class Story extends Component {
    static displayName = Story.name;


    render() {

        
        return (
            <div className="card text-center">
                <div className="card-header">
                    Your Generated MadLib!
                </div>
                <div className="card-body">
                    <h5 className="card-title">
                        The <span className="adjective">{this.props.inputAdjective}</span> man explored <span className="place">{this.props.inputPlace}</span> all while he was <span className="verb">{this.props.inputVerb}</span> the <span class="noun">{this.props.inputNoun}</span>.
                    </h5>
                </div>
                <div className="card-footer text-muted">
                    Any good? Give it another try by hitting Reset!
                </div>
            </div>
            
            
        );
    }
}
