import React, { Component } from 'react';

import { Story } from './Story';

import './story.css'

export class Home extends Component {
    static displayName = Home.name;

    constructor(props) {
        super(props);

        this.state = {
            input_noun: '',
            input_verb: '',
            input_adjective: '',
            input_place: '',
            showStory: false
        };
    }

    handleNounChange = e => {
        this.setState({ input_noun: e.target.value });
    };

    handleVerbChange = e => {
        this.setState({ input_verb: e.target.value });
    };

    handlePlaceChange = e => {
        this.setState({ input_place: e.target.value });
    };

    handleAdjectiveChange = e => {
        this.setState({ input_adjective: e.target.value });
    };

    handleSubmit = e => {
        this.setState({ showStory: true });
    };

    handleReset = e => {
        this.setState({
            showStory: false,
            input_noun: '',
            input_adjective: '',
            input_verb: '',
            input_place: ''
        });
    };

    render() {
        return (
            <div>

                <div className="container w-75 mx-auto">

                    <h3 className="display-5 text-center">Please enter your Mad-Lib words</h3>
                    <hr /><br />

                    <div className="col-12 justify-content-center">
                        <div className="input-group">
                            <div className="col-4">
                                <div className="input-group-text noun">Please enter a Noun: </div>
                            </div>
                            <input type="text" id="txtNoun" placeholder="Noun"
                                className="form-control col-8"
                                value={this.state.input_noun}
                                onChange={this.handleNounChange}
                            />

                        </div>
                    </div>
                    <br />
                    <div className="col-12 justify-content-center">
                        <div className="input-group">
                            <div className="col-4">
                                <div className="input-group-text verb">Please enter a Verb: </div>
                            </div>
                            <input type="text" id="txtVerb" placeholder="Verb"
                                className="form-control"
                                value={this.state.input_verb}
                                onChange={this.handleVerbChange}
                            />

                        </div>
                    </div>
                    <br />
                    <div className="col-12 justify-content-center">
                        <div className="input-group">
                            <div className="col-4">
                                <div className="input-group-text place">Please enter an Place: </div>
                            </div>
                            <input type="text" id="txtPlace" placeholder="Place"
                                className="form-control"
                                value={this.state.input_place}
                                onChange={this.handlePlaceChange}
                            />

                        </div>
                    </div>
                    <br />
                    <div className="col-12 justify-content-center">
                        <div className="input-group">
                            <div className="col-4">
                                <div className="input-group-text adjective">Please enter an Adjective: </div>
                            </div>
                            <input type="text" id="txtAdjective" placeholder="Adjective"
                                className="form-control"
                                value={this.state.input_adjective}
                                onChange={this.handleAdjectiveChange}
                            />

                        </div>
                    </div>
                    <br /><br />

                    <div className="row">
                        <div className="col-6">
                            <input type="button" value="Generate my Madlib!" className="btn btn-success btn-block" onClick={this.handleSubmit} />
                        </div>
                        <div className="col-6">
                            <input type="button" value="Reset" className="btn btn-danger btn-block" onClick={this.handleReset} />
                        </div>
                    </div>
                    <br />

                    {this.state.showStory ?
                        <Story inputNoun={this.state.input_noun} inputAdjective={this.state.input_adjective} inputVerb={this.state.input_verb} inputPlace={this.state.input_place} />
                        : null
                    }
                </div>
            </div>

        );
    }
}
